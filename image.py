#-*- coding:utf-8 -*-
import sys
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import math
import datetime
import os
import requests
from io import BytesIO

# image: 图片  text：要添加的文本 font：字体
def add_text_to_image(bg_imag_path,name):
    im = Image.open(bg_imag_path)
    fontSize = 34
    font = ImageFont.truetype('/www/wwwroot/msyh.ttf', fontSize)
    draw = ImageDraw.Draw(im)
    text = unicode(name,'utf-8')
    textW,textH = font.getsize(text)
    #draw.text((math.ceil(im.size[0]/2), 137), text, fill=(253, 253, 253), font=font)
    #640 1024
    draw.text(((im.size[0]-textW)/2,137), text, fill=(253, 253, 253), font=font)

    dir = "/www/wwwroot/python/"
    name = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    filename = dir + name + '_target.jpg'
    im.save(filename)
    return filename
#给图像加水印    image 图像  watermark 水印图片
def add_watermark_to_image(image, watermark,watermark_width,watermark_height,target_location_x,target_location_y):
    dir = "/www/wwwroot/python/"
    name = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    watermark = Image.open(watermark)
    #将二维码缩放到目标尺寸
    new_watermark = watermark.resize((watermark_width,watermark_height))
    filename = dir + name + '_suofang.png'
    new_watermark.save(filename)
    watermark = Image.open(filename)
    imageFile = Image.open(image)
    layer = Image.new('RGBA', imageFile.size, (0,0,0,0))

    #合并图像   水印的位置
    layer.paste(new_watermark, (target_location_x, target_location_y))
    out=Image.composite(layer,imageFile,layer)

    filename = dir + name + '_watermark.jpg'
    out.save(filename)
    return filename


#将头像生成圆形
def circle(headimgurl,width,height):
    ima = Image.open(headimgurl).convert("RGBA")
    ima = ima.resize((width, height), Image.ANTIALIAS)
    size = ima.size

    # 因为是要圆形，所以需要正方形的图片
    r2 = min(size[0], size[1])
    if size[0] != size[1]:
        ima = ima.resize((r2, r2), Image.ANTIALIAS)

    # 最后生成圆的半径
    r3 = 60
    imb = Image.new('RGBA', (r3*2, r3*2),(255,255,255,0))
    pima = ima.load()  # 像素的访问对象
    pimb = imb.load()
    r = float(r2/2) #圆心横坐标

    for i in range(r2):
        for j in range(r2):
            lx = abs(i-r) #到圆心距离的横坐标
            ly = abs(j-r)#到圆心距离的纵坐标
            l  = (pow(lx,2) + pow(ly,2))** 0.5  # 三角函数 半径

            if l < r3:
                pimb[i-(r-r3),j-(r-r3)] = pima[i,j]
    dir = "/www/wwwroot/python/"
    name = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    filename = dir + name + '_test_circle.png'
    imb.save(filename)
    return filename

#将网络图片下载到本地
def curlImage(img_src):
    response = requests.get(img_src)
    image = Image.open(BytesIO(response.content))
    dir = "/www/wwwroot/python/"
    name = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    filename = dir + name + '_img_src.png'
    image.save(filename)
    return filename

# if __name__ == "__main__":
#给背景图片上面加文字
image = add_text_to_image(bg_imag_path=sys.argv[1],name = sys.argv[4] )
#给已加文字后的背景图上面加二维码
image = add_watermark_to_image(image,watermark = sys.argv[3],watermark_width=110,watermark_height=110,target_location_x=18,target_location_y=896 )
#拉去网络头像到本地
headimgurl = curlImage(img_src=sys.argv[2])
#先判断头像需不需要放大  此处偷懒直接放大到目标尺寸了
#将头像缩放值固定尺寸
headimgurl = circle(headimgurl,width=200,height=200)
#将头像合成到背景图上面
send_img_path = add_watermark_to_image(image,watermark=headimgurl,watermark_width=110,watermark_height=110,target_location_x=265,target_location_y=495)
print(send_img_path)
print(send_img_path)